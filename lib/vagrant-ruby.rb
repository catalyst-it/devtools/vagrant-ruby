#
# Copyright (c) 2016 Catalyst.net Ltd
#
# This file is part of vagrant-ruby.
#
# vagrant-ruby is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-ruby is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-ruby. If not, see <http://www.gnu.org/licenses/>.
#

require 'rubygems'

module Vagrant
  module Ruby
    COMMANDS = %w{gem irb rake rdoc}

    class Command < Vagrant.plugin('2', :command)
      attr_reader :name
      attr_reader :flags

      def initialize(name)
        @name = name
      end

      def new(flags, *_)
        @flags = flags
        self
      end

      def synopsis
        "execute Vagrant's embedded #{name}"
      end

      def with_clean_env
        if not defined? ::Bundler
          yield
        else
          ::Bundler.with_clean_env do
            yield
          end
        end
      end

      def execute
        with_clean_env do
          command = File.join(Gem.default_bindir, name)
          Process.exec([command, name], *flags)
        end
      end
    end

    class Ruby < Command
      def execute
        preflags, subcommand, subflags = split_main_and_subcommand(flags)
        case subcommand
        when 'exec'
          Command.new(subflags.first).new(subflags.slice(1..-1)).execute
        when 'ruby'
          Command.new('ruby').new(preflags + subflags).execute
        else
          Command.new('ruby').new(flags).execute
        end
      end
    end

    class Plugin < Vagrant.plugin('2')
      name 'ruby'

      command('ruby') do
        Ruby.new('ruby')
      end

      COMMANDS.each do |name|
        command(name) do
          Command.new(name)
        end
      end
    end
  end
end
