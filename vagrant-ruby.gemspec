#
# Copyright (c) 2016-2017 Catalyst.net Ltd
#
# This file is part of vagrant-ruby.
#
# vagrant-ruby is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-ruby is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-ruby. If not, see <http://www.gnu.org/licenses/>.
#

$: << File.expand_path('../lib', __FILE__)

require 'vagrant-ruby/version'

Gem::Specification.new do |spec|
  spec.name          = Vagrant::Ruby::NAME
  spec.version       = Vagrant::Ruby::VERSION
  spec.license       = 'GPL-3.0'
  spec.author        = 'Evan Hanson'
  spec.email         = 'evanh@catalyst.net.nz'
  spec.summary       = "Use Vagrant's embedded Ruby commands"
  spec.description   = "Adds subcommands for Vagrant's embedded Ruby and associated programs"
  spec.homepage      = 'https://gitlab.com/catalyst-it/vagrant-ruby'
  spec.files         = Dir['lib/**/*.rb']
end
