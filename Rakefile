#
# Copyright (c) 2016-2017 Catalyst.net Ltd
#
# This file is part of vagrant-ruby.
#
# vagrant-ruby is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-ruby is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-ruby. If not, see <http://www.gnu.org/licenses/>.
#

$: << File.expand_path('../lib', __FILE__)

require 'bundler/gem_tasks'
require 'rake/clean'
require 'rubygems/indexer'
require 'vagrant-ruby/version'

CLOBBER.include 'repo'

gem = "#{Vagrant::Ruby::NAME}-#{Vagrant::Ruby::VERSION}.gem"
desc "Build a local gem index containing #{gem}"
task :gemserver => [:build] do
  FileUtils.rm_rf 'repo'
  FileUtils.mkdir_p 'repo/gems'
  FileUtils.cp "pkg/#{gem}", 'repo/gems'
  Dir.chdir 'repo' do
    Gem::Indexer.new('.').generate_index
  end
end

desc 'Alias for "rake gemserver"'
task :gem => :gemserver
